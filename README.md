# Aviation tool application

options:
- searching aerodrome by typing name of the city
- printing METARs and TAF by typing a ICAO code

used services:
- https://aviationweather.gov/adds/dataserver
- https://openflights.org/data.html#airport