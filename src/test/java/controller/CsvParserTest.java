package controller;

import data.Airport;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Java6Assertions.tuple;


/**
 * Created by Przemysław on 2017-05-21.
 */
public class CsvParserTest {

    CsvParser csvParser = new CsvParser();
    private List<String> airports;
    List<Airport> allAerodromesList = new ArrayList<Airport>();

    public CsvParserTest() throws Exception {
    }

    @Before
    public void setup() {
        String string1 = "1,\"Goroka Airport\",\"Goroka\",\"Papua New Guinea\",\"GKA\",\"AYGA\",-6.081689834590001,145.391998291,5282,10,\"U\",\"Pacific/Port_Moresby\",\"airport\",\"OurAirports\"";
        String string2 = "2,\"Madang Airport\",\"Madang\",\"Papua New Guinea\",\"MAG\",\"AYMD\",-5.20707988739,145.789001465,20,10,\"U\",\"Pacific/Port_Moresby\",\"airport\",\"OurAirports\"";
        String string3 = "3,\"Mount Hagen Kagamuga Airport\",\"Mount Hagen\",\"Papua New Guinea\",\"HGU\",\"AYMH\",-5.826789855957031,144.29600524902344,5388,10,\"U\",\"Pacific/Port_Moresby\",\"airport\",\"OurAirports\"";
        airports = new ArrayList<String>();

        airports.add(0, string1);
        airports.add(1, string2);
        airports.add(2, string3);
    }

    @Test
    public void createAirportListTest() throws Exception {
        allAerodromesList = csvParser.createAirportListWithTables(airports);


        assertThat(airports).hasSize(3);
        assertThat(allAerodromesList.get(0).getAirportId()).isEqualTo(1);
        assertThat(allAerodromesList.get(1).getCountry()).isEqualTo("Papua New Guinea");
        assertThat(allAerodromesList).extracting("city")
                .contains("Goroka", "Mount Hagen", "Madang");
        assertThat(allAerodromesList).extracting("iata", "icao")
                .contains(tuple("GKA", "AYGA"),
                        tuple("MAG", "AYMD"),
                        tuple("HGU", "AYMH"));



    }
}