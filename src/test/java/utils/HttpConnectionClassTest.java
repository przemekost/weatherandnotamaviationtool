package utils;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import services.ClientService;


import static org.assertj.core.api.Assertions.assertThat;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created by Przemysław on 2017-05-19.
 */
public class HttpConnectionClassTest {

    String urlLink = "https://raw.githubusercontent.com/jpatokal/openflights/master/data/airports.dat";
    private List<String> mockResponse;


    @Mock
    HttpConnectionClass httpConnectionClass;

    @InjectMocks
    ClientService clientService = new ClientService();


    @Before
    public void setup() {
        mockResponse = new ArrayList<String>();
        String string1 = "26,\"Kugaaruk Airport\",\"Pelly Bay\",\"Canada\",\"YBB\",\"CYBB\",68.534401,-89.808098,56,-7,\"A\",\"America/Edmonton\",\"airport\",\"OurAirports\"";
        String string2 = "3127,\"Pokhara Airport\",\"Pokhara\",\"Nepal\",\"PKR\",\"VNPK\",28.200899124145508,83.98210144042969,2712,5.75,\"N\",\"Asia/Katmandu\",\"airport\",\"OurAirports\"";
        String string3 = "8810,\"Hamburg Hbf\",\"Hamburg\",\"Germany\",\"ZMB\",\\N,53.552776,10.006683,30,1,\"E\",\"Europe/Berlin\",\"station\",\"User\"";
        mockResponse.add(0, string1);
        mockResponse.add(1, string2);
        mockResponse.add(2, string3);
    }

    @Test
    public void testURL() throws IOException {

        URL url = new URL(urlLink);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        assertEquals(HttpURLConnection.HTTP_OK, connection.getResponseCode());
    }


    @Test
    public void getListOfAirportsFromCsvAsAStringsTest() throws Exception {

        MockitoAnnotations.initMocks(this);
        when(httpConnectionClass.getListOfAirportsFromCsvAsAStrings("https://raw.githubusercontent.com/jpatokal/openflights/master/data/airports.dat")).thenReturn(mockResponse);

        List<String> result = clientService.getData(urlLink);

        List<String> expected = mockResponse;



        assertThat(result.get(0)).isEqualTo(expected.get(0));
    }

}