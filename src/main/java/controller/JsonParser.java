package controller;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import data.CityPair;
import data.CityPairs;
import data.Destination;
import data.Origin;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;


/**
 * Created by Przemysław on 2017-05-19.
 */
public class JsonParser {

    private static final String PATH = "C:\\android\\android-sdkwindows\\tools\\androidProjects\\weatherandnotamaviationtool\\src\\main\\resources/cityPairs";
    private BufferedReader br = null;
    private CityPairs cityPairs;
    private List<String> iataCodeList = new ArrayList<String>();

    public void conversionJsonToObject() throws IOException, ParseException {

        JsonReader reader = new JsonReader(new FileReader(PATH));


        Gson gson = new Gson();

        cityPairs = gson.fromJson(reader, CityPairs.class);

        if (cityPairs != null) {
            for (CityPair c : cityPairs.getCitypairs()) {
                if (c != null) {
                    System.out.println();
                    System.out.println("--------------------------------------------------------");

                    System.out.println("Origin airport: " + c.getOrigin().getAirport());
                    System.out.println("Estimated time of departure: )" + c.getOrigin().getEtd());
                    System.out.println("Destination airport: " + c.getDestination().getAirport());
                    System.out.println("Estimated time of arrival: )" + c.getDestination().getEta());
                }
            }
        }
    }
}






