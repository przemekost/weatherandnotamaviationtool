package controller;

import data.Airport;
import lombok.Getter;
import services.ClientService;
import utils.HttpConnectionClass;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Przemysław on 2017-05-18.
 */
public class CsvParser {

    public static final String URL_ADDRESS_FOR_AIRPORT_DATA = "https://raw.githubusercontent.com/jpatokal/openflights/master/data/airports.dat";
    private HttpConnectionClass httpConnection = new HttpConnectionClass();
    String[] tab = new String[httpConnection.getAirports().size()];
    ClientService clientService = new ClientService();
    List<String> airports = clientService.getData(URL_ADDRESS_FOR_AIRPORT_DATA);
    private String searchedCity;
    private List<Airport> allAerodromesList = new ArrayList<Airport>();
    @Getter
    private List<Airport> selectedAirportsByUser = new ArrayList<Airport>();
    private Airport airport;

    public CsvParser() throws Exception {
    }

    public List<Airport> createAirportListWithTables(List<String> airports) throws Exception {

        for (int i = 0; i < airports.size(); i++) {
            tab = airports.get(i).replace("\"", "").split(",");

            airport = new Airport.AirportBuilder()
                    .airportId(Integer.parseInt(tab[0]))
                    .name(tab[1])
                    .city(tab[2])
                    .country(tab[3])
                    .iata(tab[4])
                    .icao(tab[5])
                    .latitude(tab[6])
                    .longitude(tab[7])
                    .altitude(Double.parseDouble(tab[8]))
                    .timeZone(tab[9])
                    .dst(tab[10])
                    .tzDatabaseTime(tab[11])
                    .type(tab[12])
                    .source(tab[13])
                    .build();
            allAerodromesList.add(airport);
        }
        return allAerodromesList;
    }

    public String airportFromUser() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Type city in which you would like to search an aerodrome");
        searchedCity = sc.nextLine();
        char firstChar = Character.toUpperCase(searchedCity.charAt(0));
        searchedCity = firstChar + searchedCity.substring(1);

        return searchedCity;
    }


    public void printSearchedAirports() throws Exception {
        createAirportListWithTables(airports);
        airportFromUser();
        for (Airport select : allAerodromesList) {
            if (select.getCity().equals(searchedCity)) {
                selectedAirportsByUser.add(select);
            }
        }
        for (Airport aerodrome : selectedAirportsByUser) {
            System.out.println(aerodrome);
        }
    }

}
