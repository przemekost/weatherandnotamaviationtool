package controller;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by Przemysław on 2017-05-18.
 */
public class XmlParser {
    private final static int ICAO_CODE_LENGTH = 4;
    private String icaoCode;

    public XmlParser() {
        try {
            icaoSetter();

            String baseUrlAddressForMetars = "https://aviationweather.gov/adds/dataserver_current/httpparam?datasource=metars&requestType=retrieve&format=xml&mostRecentForEachStation=constraint&hoursBeforeNow=1.25&stationString=";
            String fullUrlAddressForMetar = baseUrlAddressForMetars + icaoCode;
            String baseUrlAddressForTaf = "https://aviationweather.gov/adds/dataserver_current/httpparam?dataSource=tafs&requestType=retrieve&format=xml&hoursBeforeNow=3&timeType=issue&mostRecent=true&stationString=";
            String fullUrlAddressForTaf = baseUrlAddressForTaf + icaoCode;

            documentBuilderFactoryForWeather(fullUrlAddressForMetar, "METAR");
            documentBuilderFactoryForWeather(fullUrlAddressForTaf, "TAF");

        } catch (Exception e) {
            System.out.println("nie ma takiego adresu URL");
            e.printStackTrace();
        }
    }


    private String icaoSetter() {
        Scanner sc = new Scanner(System.in);
        System.out.println("--------------------------------------------------------");
        System.out.println("Type the ICAO code");
        icaoCode = sc.nextLine();
        icaoCode = icaoCode.toUpperCase();
        while (icaoCode.length() != ICAO_CODE_LENGTH) {
            System.out.println("Wrong format! Type 4 characters of ICAO code");
            icaoCode = sc.nextLine();
        }
        return icaoCode;
    }

    public void documentBuilderFactoryForWeather(String desiredUrl, String tagName)
            throws Exception {

        try {
            URL url = new URL(desiredUrl);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse((url.openStream()));
            doc.getDocumentElement().normalize();

            System.out.println("Root element :"
                    + doc.getDocumentElement().getNodeName());
            NodeList nList = doc.getElementsByTagName(tagName);

            System.out.println("----------------------------");

            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                System.out.println("\nCurrent Element: " + nNode.getNodeName());

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;
                    if (tagName.equals("METAR")) {
                        System.out.println("Raw_text : " + eElement.getElementsByTagName("raw_text").item(0).getTextContent());
                        System.out.println("Observation time : " + eElement.getElementsByTagName("observation_time").item(0).getTextContent());
                    } else {
                        System.out.println("Raw_text : " + eElement.getElementsByTagName("raw_text").item(0).getTextContent());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
