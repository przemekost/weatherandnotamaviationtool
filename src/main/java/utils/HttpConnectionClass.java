package utils;

import lombok.Getter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Przemysław on 2017-05-17.
 */
public class HttpConnectionClass {


    @Getter
    private List<String> airports = new ArrayList<String>();

    public List<String> getListOfAirportsFromCsvAsAStrings(String urlAddress)
            throws Exception {

        BufferedReader reader = null;

        try {
            // create the HttpURLConnection
            URL url = new URL(urlAddress);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("GET");

            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String line = null;
            while ((line = reader.readLine()) != null) {
                airports.add(line);
            }
            return airports;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        }
    }


}




