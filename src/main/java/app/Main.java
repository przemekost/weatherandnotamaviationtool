package app;

import controller.CsvParser;
import controller.JsonParser;
import controller.XmlParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.IOException;


/**
 * Created by Przemysław on 2017-05-16.
 */
public class Main {

    public static void main(String[] args) throws Exception {
        CsvParser csvParser = new CsvParser();
        csvParser.printSearchedAirports();
        XmlParser xmlParser = new XmlParser();
        JsonParser jsonParser = new JsonParser();
        jsonParser.conversionJsonToObject();
    }

}
