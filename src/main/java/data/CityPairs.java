package data;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Przemysław on 2017-05-19.
 */
@Data
public class CityPairs {

    @SerializedName("city-pairs")
    private CityPair[] citypairs;
}
