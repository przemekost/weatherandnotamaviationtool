package data;

import lombok.Data;
import lombok.Getter;

/**
 * Created by Przemysław on 2017-05-17.
 */

@Data
public class Airport {

    private final int airportId;
    private final String name;
    private final String city;
    private final String country;
    private final String iata;
    private final String icao;
    private final String latitude;
    private final String longitude;
    private final double altitude;
    private final String timeZone;
    private final String dst;
    private final String tzDatabaseTime;
    private final String type;
    private final String source;

    private Airport(AirportBuilder builder) {
        this.airportId = builder.airportId;
        this.name = builder.name;
        this.city = builder.city;
        this.country = builder.country;
        this.iata = builder.iata;
        this.icao = builder.icao;
        this.latitude = builder.latitude;
        this.longitude = builder.longitude;
        this.altitude = builder.altitude;
        this.timeZone = builder.timeZone;
        this.dst = builder.dst;
        this.tzDatabaseTime = builder.tzDatabaseTime;
        this.type = builder.type;
        this.source = builder.source;
    }

    public static class AirportBuilder {
        private int airportId;
        private String name;
        private String city;
        private String country;
        private String iata;
        private String icao;
        private String latitude;
        private String longitude;
        private double altitude;
        private String timeZone;
        private String dst;
        private String tzDatabaseTime;
        private String type;
        private String source;

        public Airport build() {
            return new Airport(this);
        }

        public AirportBuilder source(String source) {
            this.source = source;
            return this;
        }

        public AirportBuilder type(String type) {
            this.type = type;
            return this;
        }

        public AirportBuilder tzDatabaseTime(String tzDatabaseTime) {
            this.tzDatabaseTime = tzDatabaseTime;
            return this;
        }

        public AirportBuilder dst(String dst) {
            this.dst = dst;
            return this;
        }

        public AirportBuilder timeZone(String timeZone) {
            this.timeZone = timeZone;
            return this;
        }

        public AirportBuilder altitude(double altitude) {
            this.altitude = altitude;
            return this;
        }

        public AirportBuilder longitude(String longitude) {
            this.longitude = longitude;
            return this;
        }

        public AirportBuilder latitude(String latitude) {
            this.latitude = latitude;
            return this;
        }

        public AirportBuilder icao(String icao) {
            this.icao = icao;
            return this;
        }

        public AirportBuilder iata(String iata) {
            this.iata = iata;
            return this;
        }

        public AirportBuilder country(String country) {
            this.country = country;
            return this;
        }
        public AirportBuilder city(String city) {
            this.city = city;
            return this;
        }
        public AirportBuilder airportId(int airportId) {
            this.airportId = airportId;
            return this;
        }

        public AirportBuilder name(String name) {
            this.name = name;
            return this;
        }

    }


}
