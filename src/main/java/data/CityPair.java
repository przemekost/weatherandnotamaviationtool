package data;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Przemysław on 2017-05-19.
 */

@Data
public class CityPair {
   private Origin origin;
   private Destination destination;

}
