package data;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

/**
 * Created by Przemysław on 2017-05-19.
 */
@Data
public class Origin {
    private String airport;
    @SerializedName("ETD")
    private String etd;
}
