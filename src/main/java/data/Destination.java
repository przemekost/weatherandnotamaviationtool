package data;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

/**
 * Created by Przemysław on 2017-05-19.
 */
@Data

public class Destination {
    private String airport;
    @SerializedName("ETA")
    private String eta;
}
